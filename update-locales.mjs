#!/usr/bin/env node
/**
 * @copyright Copyright 2021 Kevin Locke <kevin@kevinlocke.name>
 * @license MIT
 */

/* eslint-disable max-classes-per-file */

// TODO [engine:node@>=14]: import { ... } from 'fs/promises'
import { promises as fsPromises } from 'fs';
import { STATUS_CODES } from 'http';
import http2 from 'http2';
import path from 'path';
import { debuglog } from 'util';
import { Gunzip } from 'zlib';

const {
  mkdir,
  readdir,
  readFile,
  writeFile,
} = fsPromises;

const docsOrigin = 'https://docs.microsoft.com';

const debug = debuglog('update-locales');

class HttpError extends Error {
  constructor(method, url, message) {
    super(`${method} ${url}: ${message}`);
    this.method = method;
    this.url = url;
  }
}
HttpError.prototype.name = 'HttpError';

class HttpStatusError extends HttpError {
  constructor(method, url, headers) {
    const { ':status': status } = headers;
    super(method, url, `${status} ${STATUS_CODES[status]}`);
    this.status = status;
  }
}
HttpStatusError.prototype.name = 'HttpStatusError';

function getHtml(client, urlPath) {
  return new Promise((resolve, reject) => {
    const method = 'GET';
    const url = docsOrigin + urlPath;
    debug('Request', method, url);
    const req = client.request(
      {
        ':method': method,
        ':path': urlPath,
        'accept-encoding': 'gzip',
      },
      { endStream: true },
    );
    req.once('error', reject);
    req.once('response', (headers) => {
      debug('Response', method, url, headers);

      const status = headers[':status'];
      if (status < 200 || status >= 300) {
        req.destroy(new HttpStatusError(method, url, headers));
        return;
      }

      // FIXME: Use a proper parser: https://github.com/jshttp/content-type
      const contentType = headers['content-type'];
      if (!/^text\/html[ \t]*(?:;|$)/.test(contentType)) {
        req.destroy(new HttpError(
          method,
          url,
          `Expected Content-Type: text/html, got ${contentType}`,
        ));
        return;
      }

      const contentEncoding = headers['content-encoding'];
      // Content codings are case-insensitive
      // https://datatracker.ietf.org/doc/html/rfc7231#section-3.1.2.1
      // identity is a reserved synonym for "no encoding"
      // https://datatracker.ietf.org/doc/html/rfc7231#section-8.4.2
      const normContentEncoding =
        contentEncoding ? contentEncoding.toLowerCase() : 'identity';
      let decoded;
      switch (normContentEncoding) {
        case 'gzip':
          decoded = req.pipe(new Gunzip());
          decoded.once('error', (err) => req.destroy(err));
          break;
        case 'identity':
          decoded = req;
          break;
        default:
          req.destroy(new HttpError(
            method,
            url,
            `Unrecognized Content-Encoding: ${contentEncoding}`,
          ));
          return;
      }

      let encoding;
      if (contentType) {
        // FIXME: Use a proper parser: https://github.com/jshttp/content-type
        const charsetMatch =
          /^text\/html[ \t]*[;&][ \t]*charset[ \t]*=[ \t]*([A-Za-z0-9-]+)[ \t]*(?:[;&]|$)/
            .exec(contentType);
        if (charsetMatch) {
          encoding = charsetMatch[1];
        } else if (contentType && contentType.includes('charset')) {
          req.destroy(new HttpError(
            method,
            url,
            `Error parsing charset in Content-Type: ${contentType}`,
          ));
          return;
        }
      }

      // TODO: Implement HTML5 encoding sniffing algorithm
      // https://html.spec.whatwg.org/multipage/parsing.html#determining-the-character-encoding
      try {
        decoded.setEncoding(encoding || 'utf8');
      } catch (errEncoding) {
        req.destroy(errEncoding);
        return;
      }

      let html = '';
      decoded.on('data', (data) => { html += data; });
      decoded.once('end', () => {
        if (!encoding && !html.includes('<meta charset="utf-8"')) {
          reject(new HttpError(
            method,
            url,
            'Expected HTML to contain \'<meta charset="utf-8"\'',
          ));
          return;
        }

        resolve(html);
      });
    });
  });
}

async function getLocaleTitle(client, localeName) {
  const urlPath = `/${localeName}/dotnet/api/`;
  const html = await getHtml(client, urlPath);
  const match = /<meta property="og:title" content="([^"]+)"/.exec(html)
    || /<title>([^<]*?)(?:\s|\s[^<])?<\/title>/.exec(html)
    || /<h1[^>]*>([^<]+)<\/h1>/.exec(html);
  if (!match) {
    throw new Error(`Title not found on ${docsOrigin}${urlPath}`);
  }

  return match[1];
}

/** Gets a directory name for a given locale name.
 *
 * Language tags/subtags in directory names must be separated by underscores:
 * https://developer.mozilla.org/docs/Mozilla/Add-ons/WebExtensions/Internationalization
 *
 * BCP 47 is clear that language tags/subtags are case insensitive
 * https://tools.ietf.org/rfc/bcp/bcp47#section-2.1.1
 * Since locales listed on /locale/ have inconsistent capitalization,
 * normalize to lower-case in directory names for simplicity when updating.
 *
 * @param {string} localeName BCP 47 tag.
 * @returns {string} Directory name for localeName.
 */
function localeToDirName(localeName) {
  return localeName.replace(/-/g, '_').toLowerCase();
}

async function updateLocale(client, localeName) {
  const titleP = getLocaleTitle(client, localeName);

  const localePath = path.join('_locales', localeToDirName(localeName));
  const messagesPath = path.join(localePath, 'messages.json');

  let messages;
  try {
    const content = await readFile(messagesPath, { encoding: 'utf8' });
    messages = JSON.parse(content);

    if (messages === null
      || typeof messages !== 'object'
      || Array.isArray(messages)) {
      throw new Error(`${messagesPath} JSON content is not an object`);
    }
  } catch (errRead) {
    if (errRead.code !== 'ENOENT') {
      throw errRead;
    }
  }

  const title = await titleP;

  if (messages === undefined) {
    await mkdir(localePath, { recursive: true });
    messages = {};
  }

  messages.searchProviderName = {
    ...messages.searchProviderName,
    message: title,
  };
  messages.searchUrl = {
    ...messages.searchUrl,
    message:
      `https://docs.microsoft.com/${localeName}/dotnet/api/?term={searchTerms}`,
  };

  return writeFile(
    messagesPath,
    JSON.stringify(messages, undefined, 2),
  );
}

async function tryUpdateLocale(client, localeName, options) {
  try {
    await updateLocale(client, localeName);
    options.stderr.write(`Updated ${localeName}.\n`);
    return true;
  } catch (err) {
    options.stderr.write(`${err.stack}\n`);
    return false;
  }
}

async function updateLocales(argv, options) {
  if (argv.length > 2) {
    options.stderr.write('Error: Unexpected arguments.\n');
    return 1;
  }

  let unseenLocaleDirs;
  try {
    unseenLocaleDirs = new Set(await readdir('_locales'));
  } catch (errReaddir) {
    options.stderr.write(
      errReaddir.code !== 'ENOENT' ? `${errReaddir}\n`
        : 'Error: Expected _locales in current directory.\n',
    );
    return 1;
  }

  const client = http2.connect(docsOrigin, {
    settings: {
      enablePush: false,
    },
  });

  try {
    const localesPath = '/en-us/locale/';
    const localesHtml = await getHtml(client, localesPath);
    const localeMatches =
      localesHtml.matchAll(/<a(?=\s)[^>]*\slocale="([^"]*)"/g);
    const promises = [];
    for (const [, localeName] of localeMatches) {
      const promise = tryUpdateLocale(client, localeName, options);
      promises.push(promise);

      if (unseenLocaleDirs) {
        const localeDir = localeToDirName(localeName);
        unseenLocaleDirs.delete(localeDir);
      }
    }

    if (promises.length === 0) {
      options.stderr.write(
        `Error: No locales found on ${docsOrigin}${localesPath}\n`,
      );
      return 1;
    }

    const results = await Promise.all(promises);
    let exitCode = results.every(Boolean) ? 0 : 1;

    if (unseenLocaleDirs && unseenLocaleDirs.size > 0) {
      options.stderr.write(
        'Warning: The following locales were not updated:\n',
      );
      for (const unseenLocaleDir of unseenLocaleDirs) {
        options.stderr.write(`${unseenLocaleDir}\n`);
      }

      if (exitCode === 0) {
        exitCode = 2;
      }
    }

    return exitCode;
  } catch (err) {
    options.stderr.write(`${err.stack}\n`);
    return 1;
  } finally {
    client.destroy();
  }
}

// eslint-disable-next-line promise/catch-or-return
updateLocales(process.argv, process).then((exitCode) => {
  process.exitCode = exitCode;
});
